function [circularregionPixels,perimeterregion,frac_angularregion]=make_circular_region(origin,point1,point2,radius,image_size)


[theta1]=compute_angles(origin,point1);
[theta2]=compute_angles(origin,point2);


[point2]=correct_point2(origin,point1,point2,theta2,radius);
if theta1>theta2 
    theta1=theta1-2*pi
end

frac_angularregion=(theta2-theta1)/(2.0*pi);
dth=2.0*pi/360; %angular resolution in radians

radthes=theta1:dth:theta2;
xs=radius*cos(radthes)+origin(1);
ys=-radius*sin(radthes)+origin(2);
figure()

imagesize_x = image_size(1);
imagesize_y = image_size(2);

vx=[xs origin(1) xs(1)];vy=[ys origin(2) ys(1)];
perimeterregion=[transpose(vx) transpose(vy)];
circularregionPixels=poly2mask(vx,vy,imagesize_x,imagesize_x);

rr=figure(10);
imshow(circularregionPixels)
hold on
plot(vx,vy,'b','LineWidth',2)
axis([-10 1 -1 1])
hold off

%[x y] = meshgrid(1:imagesize_x, 1:imagesize_y);

%circlePixels = (y - y0).^2 + (x - x0).^2 <= radius.^2 & (y*cos(a)-x*sin(a)>=0);

function [angle]=compute_angles(origin,point)

xvect=point(1)-origin(1);
yvect=-(point(2)-origin(2));
tg=yvect*1.0/xvect;
angle=mod(atan(tg),2*pi);

if tg>0 & xvect<0 & yvect<0
    angle=mod(angle+pi,2*pi);
elseif tg<0 & xvect>0 & yvect<0
    angle=mod(angle+2*pi,2*pi);
elseif tg<0 & xvect<0 & yvect>0
    angle=mod(angle+3*pi,2*pi);
end



function [point2,theta2]=correct_point2(origin,point1,point2,theta2,radius);
y2=origin(2)-radius*sin(theta2);
x2=origin(1)+radius*cos(theta2);

point2=[x2 y2];

[theta2]=compute_angles(origin,point2);


