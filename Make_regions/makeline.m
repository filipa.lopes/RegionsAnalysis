function [linePixels]=makeline(x0,y0,x1,y1,imagesize)

imagesize_x = imagesize(2);
imagesize_y = imagesize(1);

[x y] = meshgrid(1:imagesize_y, 1:imagesize_x);

I=zeros(imagesize_y,imagesize_x);
[x y]=bresenham(x0,y0,x1,y1);

for i=1:size(x,1)
    I(y(i),x(i))=1;
end
linePixels=I;


% insertShape function would be great to use, but a package is missing for that... 
%linePixels = insertShape(I, 'line', [x0 y0 x1 y1], 'LineWidth', 5);


%B=(y1-y0)/(x1-x0);
%A=y0-B*x0;
%img=zeros(10,10)

%linePixels = A+B*x-y == 0;
%line([x0,x1],[y0,y1],'Color','w','LineWidth',2)

%img=zeros(100,100)


%h1=figure;
%hold on;
%h2=imshow(img);
%h3=line([0,100],[0,100],'Color','w','LineWidth',2)

%width=1;

%xpoints = [x0 x0 x1 x1 x0];
%ypoints = [y0-width y0+width y1+width y1+width y0-width];

%xpoints = [x0 x1];
%ypoints = [y0 y1];

%linePixels= poly2mask(xpoints, ypoints, imagesize_x, imagesize_y);

