function [data]=select_region_used_for_background(inout,data,image,fanalysis,index)
% select_region_to_study enables us to tell the region of the image to
% study around the sending cell.
% Creating the directory, if necessary.

original_image=data.first_image; % CAUTION; change that 
regionnumber=index;

regionlabel=num2str(regionnumber);
diroutdata=strcat('regionbackground_',regionlabel);
fulldiroutdata=fullfile(data.outdatapath,diroutdata) ;

ex=exist(fulldiroutdata);
if ex==0
    mkdir(data.outdatapath,diroutdata)
    disp('Directory created');
else
    disp(strcat('The directory  ',diroutdata,' already exist'));
end

figure(fanalysis);

%if inout.click_selecting_regions==1

if inout.click_selecting_regions==1
    disp(['Please select the first point for your region.'])
    [x0,y0] = ginputc(1,'Color', 'w', 'LineWidth', 2);
    origin=[x0,y0];
else
    origin=inout.tmp_background_region_pars{index}.pos0;
    x0=origin(1,1);
    y0=origin(1,2);
    
end

button=inout.defaultbackgroundregiontype;

% in case we are drawing a circle, check if we impose the radius
checkbutton=mean(button(1:6)=='Circle');

if inout.click_selecting_regions==1
    if and(checkbutton,isfield(inout,'radius_region_for_background_in_microns'))
        radius=inout.radius_region_for_background_in_microns/data.microns_per_pixel;
    else  
        disp(['Please select the second point for your region.'])

        [x1,y1] =ginputc(1,'Color', 'w', 'LineWidth', 2);
        point1=[x1,y1];
        radius=sqrt((x1-x0)^2+(y1-y0)^2);
        data.regionbackground{regionnumber}.pos1=point1;
    end
else
   radius=inout.tmp_background_region_pars{index}.radius;
end


switch button 
    case 'Circle'
        circlemark=make_circlemark(radius,x0,y0,image(:,:,3));
        imaux=image(:,:,3);
        image(:,:,3)=max(double(image(:,:,3)),circlemark);        
        is=imshow(image);
        
        disp([' Done, full Circle region selected.'])
        circlePixels=makecircle(radius,x0,y0,data.image_size);

        data.regionbackground{regionnumber}.regionmask=circlePixels;

    case 'Rectangle'
        
        linemark=makeline(x0,y0,x1,y0,size(image(:,:,3)));

        imaux=image(:,:,3);

        image(:,:,3)=max(double(image(:,:,3)),linemark);
        is=imshow(image);   

        disp([' Oks, please select a third point for limiting the rectangle width.'])
        [x2,y2] = ginputc(1,'Color', 'w', 'LineWidth', 2);
        point2=[x2 y2];

        image_size=size(image);
        [rectanglePixels,markrectanglePixels]=makerectangle(x0,y0,x1,y1,x2,y2,data.image_size);
        image(:,:,3)=max(double(image(:,:,3)),markrectanglePixels);

        is=imshow(image);
        hold on
        data.regionbackground{regionnumber}.pos1=point1;

        data.regionbackground{regionnumber}.pos2=point2;
        data.regionbackground{regionnumber}.regionmask=rectanglePixels;
end 

regionmask_Pixels=data.regionbackground{regionnumber}.regionmask;
 
if length(size(original_image))==2
    numpixels_mask=sum(sum(regionmask_Pixels));
    sumbackground=sum(sum(double(regionmask_Pixels).*double(original_image)));
elseif length(size(original_image))==3
    regionmask_Pixels=repmat(regionmask_Pixels,1,1,size(original_image,3));
    numpixels_mask=sum(sum(sum(regionmask_Pixels)));
    sumbackground=sum(sum(sum(double(regionmask_Pixels).*double(original_image))));
end

data.regionbackground{regionnumber}.numpixels_mask=numpixels_mask;
meanbackground=sumbackground/numpixels_mask;
data.regionbackground{regionnumber}.backgroundlevel=meanbackground;


hold off

if inout.islsm==1 %computing background levels in both zmax and zsum for orthos
    imaux=data.zsum;
    sumbackground=sum(sum(double(regionmask_Pixels).*double(imaux)));
    meanbackground=sumbackground/numpixels_mask;
    data.regionbackground{regionnumber}.backgroundlevel_zsum=meanbackground;

    imaux=data.zmax;
    sumbackground=sum(sum(double(regionmask_Pixels).*double(imaux)));
    meanbackground=sumbackground/numpixels_mask;
    data.regionbackground{regionnumber}.backgroundlevel_zmax=meanbackground;
end


%imshow(bw)
%hold on
%plot(vx,vy,'b','LineWidth',2)
%axis([-10 1 -1 1])

%hold off

data.regionbackground{regionnumber}.label=regionlabel;

data.regionbackground{regionnumber}.origin=origin;
data.regionbackground{regionnumber}.radius=[radius];
data.backgroundregionlist=unique([data.backgroundregionlist regionnumber]);
data.backgroundregionlisttemp=unique([data.backgroundregionlisttemp regionnumber]);

data.image_with_background_regions=image;








