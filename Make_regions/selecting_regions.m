function [data] = selecting_regions(inout,data,image,f3)
% Selecting_regions
inout.outdatapath=data.outdatapath; % such that in inout is recorded where are we outputing the regions outcome

data.regionlisttemp=[];

if inout.ask_numberregions==1
    inout.defaultnumberregions=1;
    prompt = {'Enter the number of regions you want to select in each image '};
    dlg_title = 'Number of regions';
    num_lines = 2;
    defaultanswer = {num2str(inout.defaultnumberregions)};
    options.WindowStyle='normal';
    options.Interpreter='tex';
    answer = inputdlg(prompt, dlg_title, num_lines, defaultanswer, options);
    number_of_regions=str2num(answer{1});
    inout.defaultnumberregions=number_of_regions;
else
    number_of_regions=inout.defaultnumberregions;
    
    if isfield(inout,'get_polygon_from_previous_data')
        if inout.get_polygon_from_previous_data==1 %
            ex=exist(fullfile(data.outdatapath,[inout.data_structure_filename '.mat']));
            if ex==0 
               msg=['No data.mat file found for',data.outdatapath,', the origin and radius of your regions could not be reused.']; 
               error(msg);
            end
            dataaux=import_data(data.outdatapath,inout.data_structure_filename);
            number_of_regions=size(dataaux.regionlist,2);
            if and(isfield(inout,'double_parabola'),number_of_regions==inout.defaultnumberregions*2)  
                if inout.double_parabola==1  
                    number_of_regions=number_of_regions/2;
                end
            end
        
        end
    end
    if isfield(inout,'get_centroids_from_found_domains')
        if inout.get_centroids_from_found_domains==1
           number_of_regions=data.number_of_selected_domains;
        end
    end    
end

if and(isfield(data,'regionlist'),~isempty(data.regionlist))
    index=max(data.regionlist)+1;
else
    index=1;
end

switch inout.number_of_regions_setting
    case 'by_number'
        for i=1:number_of_regions
        [data]=select_region_to_study(inout,data,image,f3,index);
        index=index+1;
        image=data.image_with_regions;
        end

    case 'on_the_fly'

        keep_marking_regions=1;
        number_of_regions=0;

        while keep_marking_regions
            [data,saveregion]=select_region_to_study(inout,data,image,f3,index);
            if saveregion==1
                index=index+1;
                image=data.image_with_regions;
                number_of_regions=number_of_regions+1;
            end
            disp('Press space for marking another region, and "q" for finishing marking ROIs');
            waitforbuttonpress;
            cc=get(f3,'currentcharacter');
            if cc==' '
                keep_marking_regions=1;
            elseif cc=='q'
                keep_marking_regions=0;
            end
            
            if inout.save_while_selecting_regions==1
                save(fullfile(data.outdatapath,inout.data_structure_filename),'data');
                save(fullfile(inout.datapath,'inout'),'inout');
            end
            save(fullfile(inout.outdatapath,'inout'),'inout');

            if inout.save_while_selecting_regions==1
                if mod(index,inout.save_backup_regions_step)==0
                   save(fullfile(data.outdatapath,['aux' inout.data_structure_filename]),'data');
                   save(fullfile(inout.datapath,'auxinout'),'inout');
                end
            end
        end
end


