function [image,information]=work_on_orthos(inout,cellimage,image,information)

%     figure();
%     imshow(mat2gray(max(image,[],3)));

    if strcmp(inout.defaultregiontype,'Polygon') 
        [X]=[3 2 1];
        information.Orientation='Vertical';     
    else
        [X]=find_certain_ortho(inout,cellimage);      

        if(X(1)==3)           
            information.Orientation='Horizontal';
        else
            information.Orientation='Vertical';         
        end
    end
    
    image=permute(image,X);
    dummy=information.Dimensions;
    information.Dimensions=dummy(X,:);
        
%     figure();
%     imshow(mat2gray(max(image,[],3)));

%     cellimage=permute(cellimage,X);  
%     figure()
%     imshow(mat2gray(max(cellimage,[],3)));
end