function [data]=find_all_domains(inout,data)

% Note we resize the orthos sum and max intensity projections in the xy
% direction, so that the zresolution is the same in all directions.

if isfield(inout,'outsu_threshold_prefactor')
    data.outsu_threshold_prefactor=inout.outsu_threshold_prefactor;
end

% note the imsum thresholding is performed in the normalised image

xresolution=data.resolution(1);

finalsigma=inout.gauss_sigma_microns/xresolution ;

data.xy_centroid=size(data.zsum)/2;

imsum=data.zsum;
%imsum=mat2gray(imsum); % note here we are normalising !
%imsum=imgaussfilt(imsum,finalsigma);
%level_sum = graythresh(imsum);

%name=strcat('zsum_bw_',data.rootstring_first_image_name,'.tiff');
%outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
%BW = im2bw(imsum,level_sum*inout.outsu_threshold_prefactor);
%imwrite(BW,outfilename);

immax=data.zmax;
%immax(immax<55)=0;

immax=imgaussfilt(immax,finalsigma);
level_zmax = graythresh(immax);

name=strcat('zmax_bw_',data.rootstring_first_image_name,'.tiff');
outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
BW = im2bw(immax,level_zmax*inout.outsu_threshold_prefactor);

imwrite(BW,outfilename);

image_size=size(data.zmax);

L1= bwlabel(BW);
r=regionprops(BW,'Area','Centroid','MajorAxisLength', ...
    'MinorAxisLength');

for i=1:length(r)
r(i).Distcenter=sqrt((data.xy_centroid(1)-r(i).Centroid(1))^2.0+...
    (data.xy_centroid(2)-r(i).Centroid(2))^2.0);
end


[x_cm y_cm]=find_CM_central_ROIs(r,image_size);
[r]=get_dist_cm(r,x_cm,y_cm);

% Finding index of the closest domain to the center of the image
if length(r)>1
    [mn Idx_min]=min([r.DistCM]);
   % [mn Idx_min]=min([r.Distcenter]);
   % [mx Idx_max]=max([r.Area]);
  %  if Idx_max~=Idx_max
  %      error('central domain not well detected')
  %  end
else
    error('central domain not well detected')
    Idx_min=1;
end

data.central_domain.centroid=(r(Idx_min).Centroid);

[sortedDistances, sortedInds] = sort([r.Distcenter],'descend');

%all_domains_indices=1:length(r);
all_domains_indices=sortedInds;

if inout.exclude_central_domain==1
    selected_indices_domains_indices=find(all_domains_indices~=Idx_min); % find indices of indices that are not the one from the central domain
    selected_indices=all_domains_indices(selected_indices_domains_indices); % get the indices
else
    selected_indices=all_domains_indices;
end

data.number_of_selected_domains=length(selected_indices);

for i=1:data.number_of_selected_domains
   ij=selected_indices(i);
   data.domain{i}.centroid=(r(ij).Centroid);
   data.domain{i}.majoraxislength=(r(ij).MajorAxisLength);
   
   distcenter=sqrt((data.domain{i}.centroid(1)-data.central_domain.centroid(1))^2.0+...
    (data.domain{i}.centroid(2)-data.central_domain.centroid(2))^2.0);
   data.domain{i}.distcenter_in_um=distcenter*data.resolution(1);
end

inout.plot_domains=1;

if inout.plot_domains==1
h=figure(2);
imshow(L1);
s=regionprops(L1, 'Orientation', 'MajorAxisLength', ...
    'MinorAxisLength', 'Eccentricity', 'Centroid');

% Drawing the elipse
t = linspace(0,2*pi,50);
hold on
for k = 1:length(s)
    a = s(k).MajorAxisLength/2;
    b = s(k).MinorAxisLength/2;
    Xc = s(k).Centroid(1);
    Yc = s(k).Centroid(2);
    phi = deg2rad(-s(k).Orientation);
    x = Xc + a*cos(t)*cos(phi) - b*sin(t)*sin(phi);
    y = Yc + a*cos(t)*sin(phi) + b*sin(t)*cos(phi);
    plot(x,y,'r','Linewidth',2)
end
hold off

name=strcat('domains_zmax_seg_elipse_',data.rootstring_first_image_name,'.jpeg');
outfilename=fullfile(inout.out_tiffs_xy_domain_processed_path,name);
saveas(h,outfilename)
end

end
