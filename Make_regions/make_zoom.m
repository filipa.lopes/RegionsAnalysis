function [regionmask]=make_zoom(inout,image)  

        pos=round(ginput(1)); % position to do the zoom
       % adapted from schnitzcells, manual_kant
       Lout=int8(zeros(size(image,1),size(image,2)));
        phin=image;
       zoomrect=[max([1,pos(1,2)-70]),min([size(Lout,1),pos(1,2)+69]),...
        max([1,pos(1,1)-70]),min([size(Lout,2),pos(1,1)+69])];
        Lzoom=Lout(zoomrect(1):zoomrect(2),zoomrect(3):zoomrect(4));
        Phzoom=phin(zoomrect(1):zoomrect(2),zoomrect(3):zoomrect(4));
        addfig=figure;

        LZedge = zeros(size(Lzoom));
        for ie = 1:max(max(Lzoom));
            LZedge = LZedge | bwperim(Lzoom==ie);
        end;
        LZedge=double(+LZedge);
        aux=makergb(+imresize(LZedge,5,'nearest'),imresize(Phzoom(:,:,1),5));
        imshow(mat2gray(aux(:,:,2)),'InitialMagnification', inout.zoom_magnification,'Colormap',morgenstemning);
        %imshow(aux,'InitialMagnification', inout.zoom_magnification);

        figure(addfig); % needed in case current figure changes (in Windows)
        
        if inout.draw_freehand_polygon==1 
                disp('Draw the region continuously.');
                h=imfreehand;
                disp('To confirm the ROI, double click the left mouse button. For deleting the ROI, right click the mouse button and select Delete.');
                wait(h);
                disp('hello')
                if  isvalid(h)
                    auxvar=createMask(h);
                    subaddcell=imresize(auxvar,1/5,'nearest')
                end
            else
                subaddcell=imresize(roipoly,1/5,'nearest');%(phin);
            end
                        
        if max(max(Lzoom(subaddcell>0)))>0
            disp('overlaps existing cell; ignored.');
        else
            Lzoom(subaddcell)=max(max(Lout))+1;
            Lout(zoomrect(1):zoomrect(2),zoomrect(3):zoomrect(4))=Lzoom;
        end
        regionmask=logical(Lout);
        close(addfig)
                     
end