## GENERAL DISCLAIMER
All modifications have been designed and tested for `.lif` format images using the following applications scripts:
```
RegionsAnalysis/Applications/Coupland/main_script_fluorescence_curve_lif.m
RegionsAnalysis/Applications/Coupland/main_script_inflorescense_meristem_lif.m
```

The behaviour with other image formats and scrips should remain unaffected by these modifications, but the stability not been thoroughly checked for all the possible image formats and scripts.

## CONTACT
Pau Casanova-Ferrer - pcasanov@math.uc3m.es

## GENERAL CODE REWORKS
**Rework/unification of the resolution:**   
Now the resolution of the different orthogonal views is controlled through the variables:   
`data.microns_per_pixel` `data.microns_per_pixel_orthos`   
and the original resolution is stored in `data.original_resolution` as `data.resolution`
will be modified when `work_on_orthos=1`.

This reorganization of how to store the resolution of a given view should work for all the file types.

**Multiple Otsu thresholding:**   
To avoid the overlapping between the meristem signal and the signal of nearby primordia a multiple Otsu thresholding has been implemented.     
This method applies the same principle that the original Otsu thresholding (minimizing intra-class intensity variance, or equivalently, maximizing inter-class variance) but obtaining, in this case, an arbitrary number of levels that can be set by the parameter `inout.num_thresholds`   
With the current data, we observed that the 3 level discretization (`inout.num_thresholds=2`) with the selection of the most bright regions is enough to avoid overlapping between meristem and primordia signals.

For `inout.num_thresholds=1` the previous behaviour is recovered and is still possible to modify by a multiplicative factor using `inout.outsu_threshold_prefactor`  
Both `inout.num_thresholds` and `inout.outsu_threshold_prefactor` are set to 1 in the default initialization of parameters.

**Meristem fluorescence region localization:**   
The localization of the meristem region on the **TOP** view uses the following assumption:
* The primordia signal, if existent, will be smaller than the meristem one and located symmetrically around the meristem.   

If one wants to study a particular mutant or gene that do not follow this assumption the efficacy of the automatic detector can not be guaranteed.

The pipeline uses a two-stage method to locate the meristem:
  1. First we calculate the centre of mass of all the central regions and select the region whose centroid is nearer to that centre of mass. At the same time, we also select the region with the largest area.
  2. If those two selected regions coincide we select that region as the meristem signal. If not, we compare the two largest regions.   
  If the second largest region has an area bigger than half the area of the largest and is closer to the centre of mass we select it as the meristem signal. If not we choose the region with the largest area.

For the localization of the meristem region on the **SIDE** view, the pipeline does not use the centre of mass. Instead uses the information regarding the meristem orientation (if existent) to evaluate the distance in regards to the axis coupled with the meristem orientation.    
For non `.lif` images, where the orientation is not implemented, the distance to the image centre is used instead.

Then in this case the two-stage method would be:
  1. First we select both the region whose centroid is nearer to the orientation axis and the region with the largest area.
  2. If those two selected regions coincide we select that region as the meristem signal. If not, we compare the two largest regions.   
  If the second largest region has an area bigger than half the area of the largest and is closer to the axis of orientation we select it as the meristem signal. If not we choose the region with the largest area.

**Improvement of the parabolic fitting:**  
The horizontal parabola fit has been eliminated because now the code always turns the data to fit the vertical orientation. 
The code also corrects the lateral tilting of the meristem during the parabolic fit. This correction is done by two different procedures depending on the value of curvature obtained with a preliminary fit over the data in the original orientation:
  * If the meristem is flat, with a curvature smaller than 0.01 (value chosen arbitrarily), the code will reduce the difference between the averages of the first and the last third of the data to "horizontalyze" the data. Then the image will be rotated in steps of 1 degree until the resulting fit has the apex located in the central third of the data or the rotation no longer reduces the difference between the averages of the first and the last third of the data.
  * If instead, the meristem has a curvature larger than 0.01, the code will maximize the $`r^2`$ of the parabolic fit to correct the meristem tilting. Equivalently to the other case, the image will be rotated in steps of 1 degree until the resulting fit does not improve the $`r^2`$ of the previous one.
This division in two differentiated methods has been made to avoid a wrong fit in the case of low curvature where the systematic tilting correction using $`r^2`$ produces a fit of the data to a lateral slope of a parabola of a much higher curvature that does not correspond to the meristem profile.

In both cases the final angle is stored in the variable `data.region{regionnumber}.fitparabola_tiltangle` and, if it is different than 0, a plot of the final fit is provided reversing the rotation used to fit the data.

Additionally, the script now allows for and additional modes to draw the meristem profile when `inout.zmax=0` and `inout.zmax_sandwich=1` where you select two slides of the stack to work on the projection of all the slides in between.
Finally, the interface for the selection of slides has also been improved and now allows for the presence of up to two fluorescence signals in the visualization of the stack as an aid for the selection together with the possibility of having the visualization already in the final analysis resolution if `inout.homogeneous_selection=1`.

**The signal intensity normalization:**   
Now fluorescence signal is properly volumetric through normalization with all 3 resolutions.

## ADDITIONAL `.lif` FEATURES
**Spherical aberration correction:**   
The script allows for a modification of the spacing between the Z slides to reduce the spherical aberration produced during the imaging of the sample. This resampling is controlled by the variable `inout.correct_Zfocus` which sets the new Z spacing:   
inout.correct_Zfocus$`=\frac{dZ_{New}}{dZ_{Old}}`$   
To obtain this value we use the ImageJ plugin: https://static-content.springer.com/esm/art%3A10.1038%2Fs41596-020-0360-2/MediaObjects/41596_2020_360_MOESM3_ESM.zip   
More info in Diel et all. Nat Protoc 15, 2773–2784 (2020) https://doi.org/10.1038/s41596-020-0360-2                                  

**Multiple lif files processing:**  
An option for opening all the `.lif` of a given directory has been added.
By setting `inout.open_multiple_lifs=1` the pipeline will process all the lif in the directory and change slightly the original behaviour of both `inout.numfiles_to_open` and `inout.init_file_to_open`.
  * With `inout.open_multiple_lifs=0`: `inout.numfiles_to_open` controls the number of images to open from a given file and `inout.init_file_to_open` the first image to open. (E.g. with `inout.numfiles_to_open=3` and `inout.init_file_to_open=2` the scrit will open the images 2-4 of the `.lif` file ) 
  * On the other hand with `inout.open_multiple_lifs=1`: `inout.numfiles_to_open` controls the number of images to open from all files and `inout.init_file_to_open` the first file to open. (E.g. with `inout.numfiles_to_open=3` and `inout.init_file_to_open=2` the scrit will open 3 images of all the files starting form the thrid one) 
  
**High-Resolution Homogenization:**   
To ease the measurements over an image is convenient to homogenize the resolution in both axes of the image to study. The XY axis of the confocal images already have the same resolution but the Z-axis usually have less resolution. Due to this, the orthogonal images have to be resized and the modified pixels are inferred using an interpolation. There are two main methods for this homogenization and both are possible in the current scrip by modifying the comparison between the X-axis and the Z-axis in RegionsAnalysis.m:
```
if data.original_resolution(1,1)>data.original_resolution(1,3)
if data.resolution(1,1)>data.resolution(1,3)
```
With the current configuration, we are using the highest resolution possible and therefore we expand the axis until reaching the other axis resolution. Despite the creation of new pixels required by this strategy, we have checked, using the **Control Paraboloid**, that this strategy produces better results than the alternative. The alternative would be to reduce the resolution of the axis with the highest by contracting the image in that axis.
This strategy will produce images with different resolutions for the original and orthogonal views when the Z resolution is the lowest one. Due to this, the code is prepared to work with images with different resolutions depending on the view of the meristem.

If one only wants to use the _High-Resolution Strategy_ the code could be greatly simplified because, in the typical case where the Z resolution is the lowest one, the resulting resolution would be equal for all views. Therefore, one could simply homogenize the stack to the maximum resolution of all the axis just after importing it. This alternative is already implemented on the system but is only used in rare cases when the resolution of the X and Y axis are different and `inout.NonEqual_XYResolution==1`.

This homogenization of the resolution uses the orientation of the meristem to detect which axis to modify.

**Control Paraboloid:**   
The option to substitute the experimental images with custom paraboloids has been added to the script. The function substitutes the experimental 3D matrix from the image with a paraboloid centred in the middle of the image. It can create paraboloids for both SIDE and TOP images.   
The paraboloids have a defined curvature (`inout.control_paraboloid_curvature=0.01;`) and tilting over the XZ plane (`inout.control_paraboloid_tilt=0;`) in order to have a control to check our measurements. It is also possible to artificially modify the resolution of the images but this option still requires modifying the commented lines on the section where the paraboloid is created (`if inout.create_control_paraboloid==1`) in `RegionsAnalysis.m`.

**Automatic TOP and meristem orientation detection:**   
The script automatically obtains the transversal view from a longitudinal point of view by detecting the border of the ZSUM projection with less signal (which would correspond to an empty region over the meristem).   
From this analysis, the code also infers if the meristem axis is oriented vertically or horizontally which will be used during posterior processes. For example, this orientation will be used to choose the proper permutation of the stack axis such that the orthogonal views have the same meristem orientation.

As this automatic detection can fail when there are large primordia that cover the meristem a switch to change manually the direction has been implemented: `inout.otherOrtho`. Then, for `inout.otherOrtho=1` the script chooses the direction not selected by the automatized detector.

**Double orthogonal parabolic fit of each meristem:**   
The script now offers the options to fit the profile of the meristem on two perpendicular planes. This option is active when `inout.double_parabola=1` and the two measurements are completely independent.
For SIDE images the parabola obtained from the original view is always labelled as region 1 and the orthogonal parabola as region 2.

 This allows the study of the meristem symmetry and doubles the number of data points obtained from a given set of properly imaged meristems.

**Meristem radial sizing from TOP (ZProject):**   
The script also has a function to measure the meristem radius directly on Matlab using a Z projection of a sandwich of slides (NOT IN USE because the MorphoGraphX protocol is more reliable)   
In this function, the transversal (TOP) view is selected and the user is asked to manually select two slides that will define the interval of slides to project to do the measurement. Then the user manually selects the centre of the meristem and the location of the first primordia on the projected image.
