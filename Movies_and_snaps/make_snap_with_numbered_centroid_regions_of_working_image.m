function make_snap_with_numbered_centroid_regions_of_working_image(inout,data)
% snap of the first working image

if isfield(inout,'selected_channel_string')
    strng=[data.rootstring_first_image_name '_' inout.selected_channel_string];
else
    strng=data.rootstring_first_image_name;
end


font=18;
name=strcat(strng,'_numbered_regions.tiff');
outfilename=fullfile(inout.out_tiffs_path,name);

namefig=strcat('allregions_snap.fig');
outfilenamefig=fullfile(data.outdatapath,namefig);

h=figure();

is=imshow(mat2gray(data.selected_working_image));
%is=imshow(mat2gray(data.selected_working_image),'Colormap',morgenstemning);

if inout.snap_regions_with_labels==1
    for ii=1:size(data.regionlist,2)
        pos=data.region{ii}.origin;
    %is = insertText(is,data.region{ii}.origin,data.region{ii}.label,'Textcolor','white','BoxOpacity',0)
%    data.image_with_regions(:,:,3)=max(data.image_with_regions(:,:,3),imag_with_regionlabels);
       
        switch inout.label_type_in_snap 
            case 'label'
                hnd1=text(pos(1)-10,pos(2),data.region{ii}.label);
                
            case 'class'
                hnd1=text(pos(1)-10,pos(2),num2str(data.region{ii}.class));
        end
        set(hnd1,'FontSize',inout.fontsize,'Color',inout.color_labels)
        if inout.plot_ROIs_in_snap_all_regions==1
            set(hnd1,'FontSize',inout.fontsize,'Color',inout.color_labels)
            %BW8 = uint16(bwperim(data.region{ii}.regionmask,8));
            BW8 = bwperim(data.region{ii}.regionmask,8);
            BW8=imdilate(BW8, strel('disk',1));
            
            mx=double(max(max(is.CData(:,:))));
            
            is.CData(:,:)=double(is.CData(:,:))+mx*double(BW8); 
        end
   end
end

hh=getframe(h);
hhh=frame2im(hh) ;
imwrite(hhh,outfilename);

%J = step(shapeInserter,I,PTS) 
%t = Tiff(outfilename,'w');
%t.write(hhh);
%t.close();

savefig(outfilenamefig)
%print -dpdf -painters epsFig
end
