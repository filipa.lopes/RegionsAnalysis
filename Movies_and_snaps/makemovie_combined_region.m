
function makemovie_combined_region(data,selected_image_set,regionnumber)

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;

maximumradius=data.region{ii}.radius;
radiusstep=data.region{ii}.radiusstep;
origin=data.region{regionnumber}.origin;
x0=origin(1);y0=origin(2);
frac_angularregion=data.region{regionnumber}.frac_angularregion;
regionmask_Pixels=data.region{regionnumber}.regionmask;

dirout=strcat('region_',num2str(regionnumber));
fluor_CFP=data.back_CFP_sub_fluor_timecourse;

font=18;
name=strcat('comb_region',num2str(regionnumber),'fracangle',num2str(frac_angularregion),'_',data.twochannels_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep));

outfilename=fullfile(data.datapath,dirout,name);

aviobj = VideoWriter(outfilename, 'Motion JPEG AVI'); aviobj.FrameRate = 5;
open(aviobj);

%
mxy=max(max(max(selected_image_set)));
mxc=max(max(max(data.back_CFP_sub_fluor_timecourse)));


h=figure();
ind=0; hwait=waitbar(0,'% of progress for movie generation in circular region'); % generates a waitbar
for k=1:data.time_frames
    waitbar(1.0*ind/data.time_frames); ind=ind+1; 

    image=selected_image_set(:,:,k);
    imageCFP=fluor_CFP(:,:,k);

    imm=zeros(size(image,1),size(image,2),3);
    imm(:,:,1)=image/mxy;
    imm(:,:,2)=image/mxy;
    
    if frac_angularregion<1
        imm(:,:,3)=imageCFP/mxc;
    elseif frac_angularregion==1
        circlemark=make_circlemark(maximumradius,x0,y0,imageCFP);
        imm(:,:,3)=max(circlemark,imageCFP/mxc);
    end

    
    imshow(imm);

    hold on

    plot(x0,y0,'ro','linewidth',1)
    hold on
    
    if frac_angularregion<1
        xs_perim=data.region{ii}.perimeterregion(:,1);
        ys_perim=data.region{ii}.perimeterregion(:,2);
        plot(xs_perim,ys_perim);
    end

    F=getframe(h);
    writeVideo(aviobj, F);
end

close(hwait);
close(aviobj);



