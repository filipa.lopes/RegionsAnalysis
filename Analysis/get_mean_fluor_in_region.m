function [mean_fluor]=get_mean_fluor_in_region(data,ii,selected_image_set)

regionmask_Pixels=data.region{ii}.regionmask;
fluor_mask=regionmask_Pixels.*selected_image_set(:,:);

mean_fluor=mean([fluor_mask(:)]);

end