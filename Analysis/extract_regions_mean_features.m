function [data]=extract_regions_mean_features(inout,data,list_of_regions);


areas=[];

for i=1:length(list_of_regions) 
regionnumber=list_of_regions(i);
ii=regionnumber;
ar=data.region{ii}.area_mask_in_pixels;
areas=[areas ar];
end
total_regions_areas=sum(areas);



for k=1:data.time_frames

intensities=[];
areas=[];

for i=1:length(list_of_regions) 
regionnumber=list_of_regions(i);
ii=regionnumber;

ys=data.region{ii}.intensity(k);
ar=data.region{ii}.area_mask_in_pixels;

areas=[areas ar];
intensities=[intensities ys];
end

data.allregions_average_intensity(k)=sum(intensities)/total_regions_areas;
end

end