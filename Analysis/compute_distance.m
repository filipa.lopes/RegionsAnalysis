function [dist]=compute_distance(point1,point2);
 
dimensions=ndims(point1);

dist=0;
for i=1:dimensions
dist=dist+(point2(i)-point1(i))^2;
end
dist=sqrt(dist);

end
