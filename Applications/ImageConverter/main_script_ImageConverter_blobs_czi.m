%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab script for preprocessing and 'converting' tiff images. 
% Note its current main function is to change the names so that they can be
% segmented by Shcnitzcells software.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.
%
% Contact: pau.formosajordan@slcu.cam.ac.uk
%
% See README.md document for further explanations about the code.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize parameters

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

% Initialization for ImageConverter

[inout]=initialize_for_ImageConverter;
inout.timecourse=auxinout.timecourse;
inout.number_tiff_channels=auxinout.number_tiff_channels;
inout.back_images=auxinout.crop_images;
inout.schnitzcells_channel_string=auxinout.schnitzcells_channel_string;
%inout.overrideformat=1
%inout.format='tiffrgb';
%%
inout.export_subfolderdatapath=0;
inout.create_regions_subfolders=0; % set it to 1 to create regions related subfolders, otherwise set it to 0.
inout.export_data=0; % set it to 1 to export the data structure, otherwise set it to 0.
%inout.ndsignal=2;
inout.export_renamed_for_schnitzcells=0; % rather for tiff images
inout.export_working_image_to_tiff_for_schnitzcells=1; % for LSM files or tiffs in non-standard format. Look above for standard format tiffs
inout.standard_format_filename=3;
%inout.scope='spiscan' % 'spiscan' when the membrane marker is in w1 and the signal is in w2 
%inout.tif_string_file_to_select='*w1*tif';
inout.open_all_positions=1;
inout.rolling_ball_background_filter=1; % set 1 to perform the rolling ball background filtering in the first_image (just one channel)
inout.ballsize=20;
%inout.rolling_ball_background_filter_in_signal=1; % set 1 to perform the rolling ball background filtering in the signal as well

% Execute read_rawadata for first reading tiff files, etc, and creating data structure

inout.open_several_files_in_directory=1;
inout.open_all_positions=1; % set it to 1 to make the full directory, and to 0 to restrict yourself to a position
inout.make_basic_radial=0;

if isfield(auxinout,'signal_channel_index')
    inout.signal_channel_index=auxinout.signal_channel_index;
    if isfield(auxinout,'marker_channel_index')
        auxinout.marker_channel_index=auxinout.marker_channel_index;
    end
    inout.zsum=auxinout.zsum;
    inout.zmax=auxinout.zmax;
    inout.standard_format_filename=0;
    inout.rolling_ball_background_filter=1;
end

%%
if inout.timecourse==1
% for timecourse
    inout.timecourse=1;
    inout.standard_format_filename=1;
    inout.export_renamed_for_schnitzcells=1;
    inout.export_working_image_to_tiff_for_schnitzcells=0;
    inout.tif_string_file_to_select='*w1*t1.tif';
end

%%

clear data
if inout.open_several_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


