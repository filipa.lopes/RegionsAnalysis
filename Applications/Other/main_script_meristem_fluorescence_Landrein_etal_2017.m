%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_meristem_fluorescece_Landrein_etal_2017 is a semi-automatised pipeline for
% finding the intensity levels of different fluorescent reporters at the shoot apical meristem. 
% The center of the domains are selected by the user. See 
% Supp Methods of Landrein et al 2017 for further details.

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:
% inout structure : provides parameters and strings in relation to input
% and output
% data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.
% dirdata structure: structure compiling the different data structures
% within a folder and some analysis of key features of the different data
% sets.

% See README.md document for further explanations about the code.

% Contact: pau.formosajordan@slcu.cam.ac.uk

% Last version: 29/03/2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This script has been used for TCS fluorescense quantification.
% This script allows the selection of a small region first for background and a second region
% to quantify the intensity. Note that make_fittings=0, so no fittings are
% done.

% The most important feature is dirdata.dataset{ii}.region{kk}.intensity,
% which is obtained by intensity_in_region function. 

% Note that inout.background_subtraction=1

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_meristemcell function to load the default parameters for the analysis. 
[inout]=initialize_meristemcell;

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)

inout.click_selecting_regions=1;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data. It just works for circular regions (it will load origin and radius used).
inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.find_central_domain=0; % set to 1 to find the central domain in the xy plane. For lsm files. Otherwise, set to 0.
inout.get_center_from_cental_domain=0; % set to 1 to find the origin from the central domain. Note that the radius of the selected region will still be set by the inout.radius_region_in_microns parameter. 

inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 


inout.radius_region_in_microns=40;     % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.
inout.radius_region_for_background_in_microns=10 ; % if this field is removed/commented with '%', then the program asks for a second point to mark the background region.
inout.ortho_thickness_in_microns=10;   % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.

inout.background_subtraction=1;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.select_background_regions=1;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1

inout.signal_channel_index=1;         %   channel to look at if lsm files
inout.zsum=1;                         %   if we want to do a zsum 
inout.zmax=0;                         %   if we want to do a zmax 
inout.gauss_sigma_microns=5;              %   sigma in microns units
inout.radiusstep_in_microns=1;            %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 

% Less important parameters
inout.fit_hole=0;                         %   for fitting TCS signal (circular gradient-like shape with a dip in the center), set it to 1, otherwise, to 0
inout.make_fittings=0;              % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.
inout.ask_numberregions=0;                %   set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.defaultnumberregions=1;             %   default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1;   %   default number of background regions to analyse in each file
inout.post_analysis=0;                    %   set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
inout.compute_int_vs_r=0                  % set to 1 to compute and plot the intensity vs r in the rectangle or circular region. Otherwise, set it to 0.

inout.plot_background_regions_stuff=0 ; % if select_background_regions, in case you want to see histograms and surface plots, set it to 1, otherwise to 0.


% Execute read_rawadata for importing tiff or lsm files, doing the
% pertinent analysis and creating the data structure.

clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end


%%

% This bit of code reads the different data.mat files within the subfolders inside
% the selected data folder, and generates the superstructure dirdata. Some
% selected features are exported into a csv file. 

% To extract dirdata set, write 
% e.g. dirdata.dataset{number}

[inout]=initialize_meristemcell;
[dirdata]=create_dirdata(inout);

%%
% For post regions selection in a directory. Finding the average exponent,
% doing the fitting again to such exponent, and exporting a new cvs file. 
[dirdata]=postanalysis_regions(inout)

