% Initialize parameters

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%addpath(genpath('/Users/pauformosa-jordan/Copy/Code/MATLAB/RegionsAnalysis'))

%%
%
% Pau initialization

[inout]=initialize_fluor_analysis;
inout.scope='spo'; % Spinning disk settings for clock movie
inout.scope='spy'; % Spinning disk for the H2B movie
inout.scope='spiscancit';
inout.scope='spi'; % Spinning disk but without Dual filters is 'spu'. Otherwise say 'spi'


inout.thresh_fluor_marker=0;  % from snap1 s1, t1.
inout.thresh_fluor_signal=0;
inout.threshold_backgroun_subtraction=1;
inout.load_data_for_background_file=0;
inout.extract_regions_mean_features=0;
inout.gradient_marker_max=1000;

inout.make_movie=0 % making movie or not
%%
% Execute read_rawadata for first reading tiff files, etc, and creating data structure
inout.max_fluor_to_plot=2500;
inout.make_basic_radial=0;
inout.plot_timecourse_signal=0; % set 1 for signal, 0 for the marker


inout.open_all_files_in_directory=0
inout.defaultregiontype='Circle';
%inout.defaultregiontype='Polygon';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;

inout.make_basic_radial=0
inout.plot_timecourse_signal=1; % set 1 for signal, 0 for the marker
inout.plot_time_in_frames=0;
inout.hours_per_frame=0.5;
inout.median_filter=0;



inout.compute_int_vs_r=0

inout.smooth_timecourse=1;
inout.smooth_in_z=0; % This is not implemented along time, just for first timepoint

inout.number_of_channels_to_compute_intensity=2;
inout.magnification=180;
inout.magnification=140;

inout.radius_region_in_microns=5;
inout.defaultnumberregions=40;
inout.number_of_regions_setting='on_the_fly';

inout.get_polygon_from_previous_data=0% if region is polygon

inout.get_center_and_radius_from_previous_data=0;
inout.click_selecting_regions=1;

%%
inout.number_of_regions_setting='by_number';

inout.get_center_and_radius_from_previous_data=1;
inout.click_selecting_regions=0;
%inout.imshow_in_fire=1

%%
inout.tiff_max_int=0;
inout.select_z_stack=1;
inout.focus_on_single_z_tiff=1;
%%
inout.make_zoom=0;
%%
clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

%[data]=RegionsAnalysis;

%%
% Execute create_dirdata for creating and loading a particular dirdata structure and playing with it
% To extract dirdata set, write 
% e.g. dirdata.dataset{number}

[dirdata,datapath]=create_dirdata;


%%
% Execute import_data loading a particular data structure and playing with it
% To extract data set, write data.featureofinterest
% e.g. data.region{number}
% e.g. data.region{1}.regionmask shows the mask of the selected region
[data]=import_data;

%%
% Given a list of regions to analize in a data structure, analysis_regions
% provide differerent tools to analyze the data.
% 3 main options for passing a lis:
% 1.- Using the list in data.regionlist, what contains all regions analysed
% for a data set.
% 2.- Using the temporary list data.regionlisttemp (updated every time one
% adds new regions)
% 3.- Using a particular list set by you, e.g. list_of_regions=[2 1]

list_of_regions=data.regionlist;
%list_of_regions=data.regionlisttemp;  % Note data.regionlisttemp 
%list_of_regions=[2,1];
[data]=analysis_regions(inout,data,list_of_regions);
%%
plot_regions_mean_features(inout,data,data.regionlist);
%%

% Use add_regions in case you have already a data structure and you want to keep it but add new regions.
% Note that add_regions function calls import_data function
inout.get_center_and_radius_from_previous_data=0;
inout.click_selecting_regions=1;
[data]=add_regions_to_study(inout,data);


%%


make_snap_all_regions(inout,data)

