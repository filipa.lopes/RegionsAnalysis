function [inout]=initialize_pinsigcell

% Parameter settings
[inout]=initialize_default_parameters; % initialize default parameters

inout.threshold_backgroun_subtraction=1;
inout.median_filter=1;
inout.plot_time_in_frames=1;

% Parameter settings

inout.export_renamed_for_schnitzcells=0;

% general parameters
inout.open_all_files_in_directory=0; % set to 1 for opening all tiffs or lsm files in a directory, 0 otherwise.


inout.extract_regions_mean_features=1;
inout.select_region=1;              % set to 1 for selecting regions, 0 otherwise
inout.background_subtraction=0;
inout.select_background_regions=0;   % set to 1 for selecting background regions, 0 otherwise

inout.snap_regions_with_labels=1;   % set to 1 for drawing the regions labelsin a figure
inout.export_subfolderdatapath=1;   % set to 1 for exporting files in a subfolder where the raw data is, 0 for exporting to the same folder as raw data 
inout.standard_format_filename=1;   % set to 0 to load TIFF files with arbitrary names
inout.export_data=1;                % set to 1 to export data structure into the output folder after reading data and do on
inout.num_channels=2;               % number of channels of your image. Please set to 1 for the moment.
inout.create_dirdata=1;             % if all files in a directory are explored, this gives the possibility to create another structure, called dirdata in the raw data directory. 
inout.defaultregiontype='Rectangle';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' and 'Cake piece'). Comment this line if different regions are needed to be explored.;
inout.defaultnumberregions=1;       % default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=1; % default number of background regions to analyse in each file

inout.scope='spin'; %inout.scope='cyano1';


% pop-up-related parameters

inout.ask_numberregions=1;          % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.ask_labelregions=0;           % set this parameter to 1 if you want matlab ask you the labels of the regions
inout.ok_boxes=1;                   % set this parameter to 1 if you want matlab poping-up 'oks' during the RegionAnalysis execution

inout.ask_numberbackgroundregions=0;

% data analysis parameters
inout.analysis_regions=0;           % set to 1 for analysing the different regions, 0 otherwise
inout.export_data_analysis=1;       % set to 1 for exporting data analysis into the data structure
inout.analysis_regions=1;

inout.radiusstep=3;                 % set the dr for doing the circle integration
inout.plotfits=0;                   % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.plotwinfit=0;                 % set to 1 to plot the resulting fittings, and to 0 otherwise.

inout.crit_conc=10;                 % critical threshold fluorescense to define a threshold-based characteristic size

inout.make_fittings=0;
inout.make_basic_radial=1;
inout.make_historegion=1;

% other stuff
inout.timecourse=1;
inout.make_movie=1;


% End of the parameters definition

currentdir=pwd; 
str = which(check_computer);
[inout.codefolder,inout.datafolder]=check_computer;

datafolder=inout.datafolder;
datapath = uigetdir(datafolder,'Select the data set location');

if ~datapath
return;
end

disp('chosen directory is');
disp(datapath)

inout.datapath=datapath;