%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% main_script_annotating_czi Matlab script simply draws different regions of
% interest (ROI) - by default, small circles - in czi files. 

% Conceived as a starting point for annotating features in cell cultures 
% (see script_classify_budding_cells.m in ProtoplastAnalysis package) 

% This script is part of the RegionsAnalysis code.

% RegionsAnalysis code mainly have three structure variables:

% 1) inout structure : provides parameters and strings in relation to input
% and output

% 2) data structures: every analysed file has its own data structure, where
% the image and its related analysis is stored.

% 3) dirdata structure: structure compiling the different data structures 
% within a folder and some analysis of key features of the different data
% sets.

% If loading the dirdada structure, you can access the regions analysis 
% by typing dirdata.dataset{i}.region{j}, being i and j two indices.

% See README.md document for further explanations about the code.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Path-related stuff
tmp = matlab.desktop.editor.getActive;
inout.applicationcodefolder=fileparts(tmp.Filename);
addpath(genpath(inout.applicationcodefolder));
[startIndex,endIndex] = regexp(inout.applicationcodefolder,'RegionsAnalysis');
inout.codefolder=inout.applicationcodefolder(1:endIndex);
addpath(genpath(inout.codefolder));

%  Execute initialize_multipolygons_twochannels function to load the default parameters for the analysis. 
[inout]=initialize_annotations;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% MOST RELEVANT PARAMETERS (note these parameters might override parameters set by the previous initialize function)
% NOTE: do not move these parameters above the initialization line.
%%

inout.defaultregiontype='Circle';
inout.ask_regionclass=1;  % set this parameter to 1 if wanting to class each region after selecting it (single key character), 0 otherwise
inout.label_type_in_snap='class';    % set 'label' or 'class'

inout.zmax=1;
inout.draw_freehand_polygon=1; % enables drawing of the polyigon in a freehand style. Otherwise set it to 0, and multiple clicks will be needed to plot the polygon.
inout.open_all_files_in_directory=0;
inout.do_files_preselection=0;
inout.get_polygon_from_previous_data=0; % set 1 to get polygons from previous data, otherwise set it to 0. Note that if setting this to 1, if you did not analyse all images from the lif project, you need to specify parameter inout.numfiles_to_open
inout.get_center_and_radius_from_previous_data=0;
inout.analysis_regions=0;

inout.make_zoom=0; % set to 1 if zoom needed for drawing ROI, and 0 otherwise. When set to 1, place cursor in the region where zoom is needed, and press click.  
inout.magnification=240; % Percentage of image magnification. Note that if this is higher than what the screen allows, it will be rest to the default value, which is 100% magnification.

inout.number_of_regions_setting='on_the_fly'; % 'on_the_fly' option allows you to not having to set the number of regions to mark in advance. When being 'on_the_fly' mode, press space bar to start selecting the next ROI, or press key 'q' to finish selecting ROIs in the current image. Otherwise, set 'by_number'.

inout.extract_features=0;
inout.signal_channel_index=2; %  Set the channel where you want to do the annotations.
inout.standard_format_filename=4; % for tiff files set 1-3, otherwise choose another number

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
inout.save_while_selecting_regions=1; % pending to fix a bud for this pipeline to save as it goes
inout.magnification=500;
clear data
if inout.open_all_files_in_directory==1
    RegionsAnalysis(inout)
else
    [data]=RegionsAnalysis(inout)
end

%% pending to revise adding regions
%add_regions_and_resume_analysis(inout.datapath)

%%
show_image_with_labels(inout,data)

