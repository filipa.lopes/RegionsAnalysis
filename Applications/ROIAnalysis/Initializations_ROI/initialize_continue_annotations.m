function [inout]=initialize_continue_annotations
% taken from initialize_annotations, with few modifications (eg not asking
% for the folder, and having the inout argumnet)


% Parameter settings
[inout]=initialize_default_parameters; % initialize default parameters

%fields(inout)
%fields(auxinout)

% Just for LIF files
%inout.numfiles_to_open=2; % Number of images to open from a lif file. Comment this line with the "%" symbol if wanting to open all images in a liff file
%inout.init_file_to_open=4; % Initial image number to open in a lif file. If this line is commented, the default number is one. Uncomment this line  if needed to make the ROIs from a certain image in the lif file onwards.  
inout.marker_channel_index_lif=2;           %   marker channel number showing cell outlines
inout.signal_channel_index_lif=1;           %   signal channel number

inout.plot_fluor_surf_2channels=0;

% Execute read_rawadata for importing lif, tiff or lsm files, doing the
% pertinent analysis and creating the data structure for each image, and the dirdata structure if necessary.
inout.number_of_channels_in_lif=2; % Number of channels the lif file has. If set to 1, the channel under study will be set to 1, overriding the channel number parameters above.
if inout.number_of_channels_in_lif==1
        inout.signal_channel_index=1;
else
        inout.signal_channel_index=inout.marker_channel_index_lif; % Do not modify this parameter for the current pipeline;
end

inout.analysis_type='Standard'; % Set it to 'Standard' or 'Multi-z'. 'Multi-z' finds an optimal z and performs computations in a sandwitch around it.
inout.label_type_in_snap='label'    % set 'label' or 'class'
inout.make_historegion=0;           % set to 1 to make histograms for the regions of interest, otherwise set to 0.
inout.defaultregiontype='Circle';   % type of region that is going to be analysed (current defaultregiontype options: 'Circle' , 'Cake piece', 'Rectangle', 'Polygon'...). Comment this line if different regions are needed to be explored.;
inout.ask_regionclass=0;  % set this parameter to 1 if wanting to class each region after selecting it (single key character), 0 otherwise
inout.imshow_in_fire=0;             % set 1 to show image in fire, 0 otherwise. 
inout.class_number_of_digits=2;
inout.find_semiaxis=1;

% general parameters
inout.click_selecting_regions=1;    % set to 1 for selecting regions by clicking, set to 0 to load it from previous data or it will find the origin of the central domain. 
inout.open_all_files_in_directory=1; % set to 1 for opening all the tiffs or lsm files in a directory, 0 otherwise.
inout.signal_channel_index=1;    % channel to look at if lsm files
inout.zsum=0;                    % if we want to do a zsum 
inout.zmax=0;                    % if we want to do a zmax 
inout.lsm_gauss_filter=0;            % if we want to do a gaussian filter
inout.gauss_sigma_microns=5;         %   sigma in microns units


inout.fit_hole=0;                         %   for fitting TCS signal (circular gradient-like shape with a dip in the center), set it to 1, otherwise, to 0
inout.r_crit_thresh=0;              % set to 1 if you want to compute a threshold-based characteristic length, otherwise set it to 0

inout.plot_ROIs_in_snap_all_regions=1; % set 1 to plot the ROIs in the file showing the ROI labels, otherwise set 0.

inout.select_region=1; % set to 1 to deal with regions of interest, otherwise set to 0.
inout.snap_regions_with_labels=1;   % set to 1 for drawing the regions labels in a figure, 0 otherwise. 
inout.export_subfolderdatapath=1;   % set to 1 for exporting files in a subfolder where the raw data is, 0 for exporting to the same folder as raw data. 
inout.standard_format_filename=0;   % set to 0 to load TIFF files with arbitrary names.
inout.export_data=1;                % set to 1 to export data structure into the output folder after reading data and so on.
inout.num_channels=1;               % number of channels of your image. Please set to 1 for the moment.
inout.create_dirdata=1;             % if all files in a directory are explored, this gives the possibility to create another structure, called dirdata in the raw data directory. 
inout.defaultbackgroundregiontype='Circle';

inout.defaultnumberregions=1;       % default number of regions to analyse in each file
inout.defaultnumberbackgroundregions=0; % default number of background regions to analyse in each file

inout.plot_background_regions_stuff=0 ; % if select_background_regions, in case you want to see histograms and surface plots, set it to 1, otherwise to 0.

inout.show_label_image=1 % 
inout.make_snap_with_numbered_centroid_regions=1; % export the raw image with labels in the centroids of the regions
inout.make_snap_of_working_images=0;  % export the working image

inout.make_orthos=0;                   % set to 1 if making ortogonal slices, otherwise set it to 0. 

inout.get_center_and_radius_from_previous_data=0; % for loading from previous data. It just works for circular regions (it will load origin and radius used).
inout.find_central_domain=0; % set to 1 to find the central domain in the xy plane. For lsm files. Otherwise, set to 0.
inout.get_center_from_cental_domain=0; % set to 1 to find the origin from the central domain. Note that the radius of the selected region will still be set by the inout.radius_region_in_microns parameter. 

% pop-up-related parameters

inout.ask_numberregions=1;          % set this parameter to 1 if you want matlab ask you how many regions to analyze per file, otherwise set it to 0. 
inout.ask_labelregions=0;           % set this parameter to 1 if you want matlab ask you the labels of the regions
inout.ok_boxes=0;                   % set this parameter to 1 if you want matlab poping-up 'oks' during the RegionAnalysis execution

inout.ask_numberbackgroundregions=0;

inout.radius_region_in_microns=1.5;     % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.
inout.radius_region_for_background_in_microns=10 ; % if this field is removed/commented with '%', then the program asks for a second point to mark the background region.
inout.ortho_thickness_in_microns=10;   % if this field is removed/commented with '%', then the program asks for a second point to mark the region to study.


% data analysis parameters
inout.analysis_regions=1;           % set to 1 for analysing the different regions, 0 otherwise.
inout.analysis_after_reading=1;     % set to 1 for analysing the data after doing all the files opening

inout.export_data_analysis=1;       % set to 1 for exporting data analysis into the data structure.
inout.analysis_regions=1;
inout.radiusstep=2;                 % set the differential radial length (dr) for doing the circle integration
inout.radiusstep_in_microns=1;      %   parameter just working for lsm, it overrides the inout.radiusstep parameter, which is in pixel units. 
inout.plotfits=1;                   % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.plotwinfit=0;                 % set to 1 to plot the resulting fittings, and to 0 otherwise.
inout.post_analysis=0;              %   set this parameter to 1 to do the fit to averaged exponent found after fittings, otherwise set to 0. Note this is possible when there is just one selected region per image.
%inout.fit_fixed_exp=1;
inout.compute_int_vs_r=0                  % set to 1 to compute and plot the intensity vs r in the rectangle or circular region. Otherwise, set it to 0.

inout.crit_conc=10;                 % critical threshold fluorescense to define a threshold-based characteristic size.
inout.make_basic_radial=1; 
inout.make_fittings=0;              % set to 1 to fit the intensity vs r curves to generalised exponentials and hill functions. Otherwise, set to 0.

inout.extract_features=1;    % set to 1 to extract the basic computed features into a csv file, otherwise set to 0.
inout.extract_features_stats=0;  % set to 1 to extract some averages of the basic computed features into a csv file, otherwise set to 0. Not functional if more than one region are selected per file.

% other stuff
inout.timecourse=0;
inout.make_movie=0;

inout.background_subtraction=0;           %   set this parameter to 1 to do the background subtraction, and to 0 otherwise.         
inout.select_background_regions=0;        %   set to 1 for selecting background regions, 0 otherwise. Note this will be subtracted from the image to analyse if inout.background_subtraction=1


% End of the parameters definition

%str = which(check_computer);
%[inout.codefolder,inout.datafolder]=check_computer;

inout.codefolder ='C:\MATLAB\RegionsAnalysis';
inout.datafolder='C:\Users\pau.formosajordan\Data_analysis_temp';
            
datafolder=inout.datafolder;
