function run_RegionsAnalysis_in_subdirectories(inout)

[dir_to_explore,main_directory]=find_all_directories_to_explore(inout);

for ii=1:size(dir_to_explore,2)
    tmp=dir_to_explore(ii);
    inout.datapath=tmp{1};
    ['Analysing ' inout.datapath ' folder']
    clear data
    if inout.open_all_files_in_directory==1
        RegionsAnalysis(inout)
    else
        [data]=RegionsAnalysis(inout)
    end
end

end