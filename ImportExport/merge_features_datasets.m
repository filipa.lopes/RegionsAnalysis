
function merge_features_datasets(inout,data1,data2);
% This function import two datasets composed of data1 and data2 instances
% and aligns its features based on the centroids of its ROIs.

% Its output is two excel files.

        [dirdata1,datapath]=import_dirdata(inout.datapath,data1);
        [dirdata2,datapath]=import_dirdata(inout.datapath,data2);
        fields1=fieldnames(dirdata1.features);
        fields2=fieldnames(dirdata2.features);

        dirdata1_vector=[];
        reordering_dirdata2_vector=[];

        for j=1:dirdata1.num_datasets
            [vect1 vect2]=align_datasets(inout,dirdata1.dataset{j},dirdata2.dataset{j});
            
            fvect1=vect1+dirdata1.cumulative_numregion_per_dataset(j);
            fvect2=vect2+dirdata2.cumulative_numregion_per_dataset(j);
            
            dirdata1_vector=[dirdata1_vector fvect1];            
            reordering_dirdata2_vector=[reordering_dirdata2_vector fvect2]; 
            make_snap_merged_datas(inout,dirdata1.dataset{j},vect1,dirdata2.dataset{j},vect2)
        end

%        final_dirdata1_vector=[dirdata1.cumulative_numregions_list+dirdata1_vector];
%        final_reordering_dirdata2_vector=[dirdata2.cumulative_numregions_list+reordering_dirdata2_vector];
        final_dirdata1_vector=dirdata1_vector;
        final_reordering_dirdata2_vector=reordering_dirdata2_vector;

        if length(final_dirdata1_vector)==length(final_reordering_dirdata2_vector)
            numrows=length(final_dirdata1_vector);
        else
            disp('bad_aligment')
        end

        for i=1:size(fields1,1)
            field=fields1{i};
            orderedfeatures1.(field)=[];
            for j=1:numrows                
                ik=final_dirdata1_vector(j);
                if iscell(dirdata1.features.(field))
                    orderedfeatures1.(field){j,1}=dirdata1.features.(field){ik,1};
                else
                    orderedfeatures1.(field)(j,1)=dirdata1.features.(field)(ik,1);
                end
            end
        end

        for i=1:size(fields2,1)
        field=fields2{i};   
            orderedfeatures2.(field)=[];
            for j=1:numrows
                ij=final_reordering_dirdata2_vector(j);
                if iscell(dirdata2.features.(field))
                    orderedfeatures2.(field){j,1}=dirdata2.features.(field){ij,1};
                else
                    orderedfeatures2.(field)(j,1)=dirdata2.features.(field)(ij,1);
                end
            end
        end

        csv_file=fullfile(datapath,['final_aligned_dir' data1 '_features.csv']);
        struct2csv(orderedfeatures1,csv_file);

        csv_file=fullfile(datapath,['final_aligned_dir' data2 '_features.csv']);
        struct2csv(orderedfeatures2,csv_file);
        
        o1=struct2table(orderedfeatures1);
        o2=struct2table(orderedfeatures2);
        
        %merged_table=join(o1(:,[1 end-1:end]),o2(:,[1 9:end]),'Keys','namefiles')
        merged_table=[o1(:,[1 end-1:end]) o2(:,9:end)];
        csv_file=fullfile(datapath,['final_merged_and_aligned_dirdata_features.csv']);
        writetable(merged_table,csv_file);
end