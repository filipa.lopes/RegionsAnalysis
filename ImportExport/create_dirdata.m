function [dirdata]=create_dirdata(inout)

% This function creates the dirdata structure by putting together the different data structures in the different subfolders.
% To access to the different data structures, you have to write dirout.dataset{index}.
% Finally, it generates an csv file with some extracted features.

%if(nargin < 1)
%datapath = uigetdir('/','Select the data set location');
%end

datapath=inout.datapath;
D=dir(datapath);

index=1;
for ifiles=1:length(D)
    fullname=D(ifiles).name;
    if isdir(fullfile(datapath,fullname))
        if not(fullname(1)=='.')
            fullfilestring=fullfile(datapath,fullname,[inout.data_structure_filename,'.mat']);
            if exist(fullfilestring,'file') % this if is to check just subfolders having data.mat files
                src=load(fullfilestring);
                if isfield(src.data,'temp_zstacks')
                    src.data=rmfield(src.data,'temp_zstacks');
                end
                dirdata.dataset{index}=src.data;
                index=index+1;
            else 
                if exist(fullfile(datapath,fullname,[inout.data_structure_filename,'_channel_2','.mat']),'file')
                    src=load(fullfile(datapath,fullname,[inout.data_structure_filename,'_channel_2','.mat']));
                    if isfield(src.data,'temp_zstacks')
                        src.data=rmfield(src.data,'temp_zstacks');
                    end
                    dirdata.dataset{index}=src.data;
                    index=index+1;
                end
                if exist(fullfile(datapath,fullname,[inout.data_structure_filename,'_channel_3','.mat']),'file')
                    src=load(fullfile(datapath,fullname,[inout.data_structure_filename,'_channel_3','.mat']));
                    if isfield(src.data,'temp_zstacks')
                        src.data=rmfield(src.data,'temp_zstacks');
                    end
                    dirdata.dataset{index}=src.data;
                    index=index+1;
                end
            end
        end
    end    
end

dirdata.datapath=datapath;

if inout.extract_features==1
    [dirdata]=extract_features(dirdata,inout);
end


if inout.extract_schnitz_features==1
    [dirdata]=extract_schnitz_features(dirdata,inout);
end

%save(fullfile(datapath,'dirdata'),'dirdata','-v7.3');
save(fullfile(datapath,['dir' inout.data_structure_filename]),'dirdata');

csv_file=fullfile(datapath,['dir' inout.data_structure_filename '_features.csv']);
struct2csv(dirdata.features,csv_file);

end
