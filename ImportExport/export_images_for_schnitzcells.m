function [data]=export_images_for_schnitzcells(inout,data)
    
    data.schnitzdatapath=fullfile(inout.datapath,'schnitz');
    create_dir(data.schnitzdatapath);
    
    if and(data.num_images>1,inout.focus_on_single_z_tiff==1)
        rootname=['snap_z',data.zstack];
    else
        rootname='snap'; 
        rootname=inout.basename;
    end
    
    time_frames=data.time_frames;

    if inout.combine_signal_and_marker_for_schnitzcells==1
        for k=1:time_frames       
            data.fluor_marker_timecourse(:,:,1,k)=max(data.fluor_signal_timecourse(:,:,1,k),data.fluor_marker_timecourse(:,:,1,k))
        end
    end
    
    for k=1:time_frames       
        timepoint=inout.ini+k-1;
        newname_signal = [rootname,'-',num2str(data.stagepos,'%1.2d'),inout.signal_schnitzcells,num2str(timepoint,'%1.3d'),'.tif'];
        fullname=fullfile(data.schnitzdatapath,newname_signal);
        imwrite(data.fluor_signal_timecourse(:,:,1,k),fullname);

        newname_marker = [rootname,'-',num2str(data.stagepos,'%1.2d'),inout.marker_schnitzcells,num2str(timepoint,'%1.3d'),'.tif'];
        fullname=fullfile(data.schnitzdatapath,newname_marker);
        imwrite(data.fluor_marker_timecourse(:,:,1,k),fullname);
                
       % system(['rename "' fullname '" ' fullname]);
    end
    
end
