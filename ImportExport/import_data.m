function [data,datapath]=import_data(datapath,data_structure_filename)
% import_data function loads the data structure form the data.mat file.

if(nargin < 1)
    datapath = uigetdir('/','Select the data set location');
    data_structure_filename='data';
end

src=load(fullfile(datapath,[data_structure_filename,'.mat']));
data=src.data;
data.datapath=datapath;

end