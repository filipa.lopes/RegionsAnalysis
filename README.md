RegionsAnalysis Matlab Package
---------------------------------------

CONTACT: pformosa@mpipz.mpg.de  

Main developers: Pau Formosa-Jordan, Pau Casanova-Ferrer.  

SUMMARY
------------
RegionsAnalysis is a Matlab package that enables to extract and analyse Regions of Interest (ROIs) from snapshots, z-stacks, or movies and store them in a data structure. This package also allows performing different image processing operations. RegionsAnalysis is articulated through different main files that perform different pipelines. Such pipelines can be applied to TIFF, LSM, CZI and LIF formats. The current possible ROIs are free-hand drawn ROIs, polygons, circles, piece of cakes, lines, horizontal rectangles and tilted rectangles.


INSTRUCTIONS at a GLANCE
-----------------------------------

To use any of the Applications, please refer to the corresponding README file within each Application subfolder.

In general, for any application, you will have to start opening  the corresponding main_script of each application and proceed with its execution.  Note that when you use it for the first time, you might have to adapt the function check_computer.

In the main_script, you can initialize the parameters by executing the corresponding initialization file.

This code relies on two main different matlab structures: inout structure, which stores the information about exporting and importing data and results, and data structure, which contains the raw data and its derived information and results.

Note that this code still work in progress. If you want to use any application different from 'Landrein et al 2018 Code', it is strongly advised to contact Pau Formosa-Jordan, to make sure the application is ready to be used by others. Any feedback about this code will be very welcome.


INSTALLATION NOTES
---------------------------

This code has been run using MATLAB (versions from 20017a until 2021a) under the following operating systems: Windows 7 Professional, Windows 10 Professional, macOS X El Capitan and macOS Mojave.

- Note for those users downloading the RegionsAnalysis code as a zip file:  

Unzip the 'RegionsAnalysis-master.zip' file, and rename the unzipped folder to just 'RegionsAnalysis' (i.e., without the '-master' substring).  

- Note for users interested in ROIsAnalysis Application:

For doing zooms on the studied images, you will need to use some external functions from Schnitzcells software that are not provided herein. To download Schnitzcells, please go to
http://easerver.caltech.edu/wordpress/schnitzcells/ . Once downloaded it, you can copy the whole packake withing the directory of External Functions, or just need to copy makergb.m, maxmax.m, minmin.m.


APPLICATIONS
--------------------

See Applications Subfolder (work in process).

- Landrein et al 2018 Code: Analysis of the central fluorescence expression domain in the Arabidopsis shoot apical meristem.

	Reference:

	Benoit Landrein, Pau Formosa-Jordan, Alice Malivert, Christoph Schuster, Charles W. Melnyk, Weibing Yang, Colin Turnbull, Elliot M. Meyerowitz, James C.W. Locke, Henrik Jönsson (2018) Nitrate modulates shoot stem cell dynamics through cytokinins. PNAS. In press.


[Note the following Applications are still work in process; documentation needs to be provided, and the code cleaned]

- ROIAnalysis: code for performing analysis of manually selected regions of interest (ROI) for LIF, LSM, CZI and Tiff files.  

- ImageConverter: Code for doing basic operations (eg zmax projections). This can rename the files so that it can be analysed by Shcnitzcells software.

- MovieGenerator: Code for putting together Tiff files from a time-lapse to generate a movie.

- Others (work in process)
* main_script_TIFF_ROIs : code for performing analysis of manually selected regions of interest (ROI) in Tiff files.
(...)



ADVANCED USERS NOTES
-------------------------------

(In process, avoid reading if possible for know…)

List of functions

Main functions

- RegionsAnalysis: this function enables to read one single tiff file or all the tiff files in a directory, and perform all the analysis.
- create_dirdata: this function creates the dirdata structure by putting together the different data structures in the different subfolders. To access to the different data structures, you have to write dirdata.dataset{index}.
dirdata.features: stores the different features from the datasets
dirdata.features_of_interest

- main_script: generic file to start with for different problems

- import_data: function to do a basic import of the data structure

Analysis
- analysis_regions: this function enables the use of different created tools to study particular selected regions along a movie.
It is executed through
A) [data]=analysis_regions(data,list_of_regions) in case you want to store some of the analysis in the current data structure
B) analysis_regions(data,list_of_regions) , in case you don’t want to store the new analysis
(so previously one has imported a data structure)

% There are 3 main options for accessing to the labels of a list of regions:
% 1.- Using the list in data.regionlist, what contains all regions analysed
% for a data set.
% 2.- Using the temporary list data.regionlisttemp (updated every time one
% adds new regions)
% 3.- Using a particular list set by you, e.g. list_of_regions=[2 1]

Note that within the analysis_regions function, one can decide whether export or not the data structure (i.e. overwrite the existing data.mat file) through a parameter.

Regions
- selecting_regions calls select_region_to_study
- select_region_to_study. It enables

Fittings
- make_nonlinfit_profile: this function fits the data into a generalized exponential profile and to a hill function, and creates the corresponding plots.
- makecircle

Analysis
- intensity_circular_region_in_time(data,regionnumber): it performs full or partial angle integration of the intensity.

External  functions:
- ginputc, from
http://www.mathworks.com/matlabcentral/fileexchange/38703-custom-ginput
- bresenham, from
http://www.mathworks.com/matlabcentral/fileexchange/28190-bresenham-optimized-for-matlab


Parameter settings

1.- General parameters

- inout.open_all_tiffs_in_directory:  set to 1 for opening all the tiffs in a directory, 0 otherwise.
- inout.select_region: set to 1 for selecting regions, 0 otherwise.
- inout.snap_regions_with_labels: set to 1 for drawing the regions labels in a figure, 0 otherwise.
- inout.export_subfolderdatapath: set to 1 for exporting files in a subfolder where the raw data is, 0 for exporting to the same folder as raw data.
- inout.standard_format_filename: set to 0 to load TIFF files with arbitrary names.
- inout.export_data: set to 1 to export data structure into the output folder after reading data and so on.
- inout.num_channels: number of channels of your image. Please set to 1 for the moment.
- inout.create_dirdata: if all files in a directory are explored, this gives the possibility to create another structure, called dirdata in the raw data directory.
- inout.defaultregiontype: type of region that is going to be analysed. Current defaultregiontype options: 'Circle' and 'Cake piece'.
Comment this line if different regions are needed to be explored.


2.- Data analysis parameters

- inout.analysis_regions: set to 1 for analysing the different regions, 0 otherwise.
- inout.export_data_analysis: set to 1 for exporting data analysis into the data structure.
- inout.analysis_regions:
- inout.radiusstep: set the differential radial length (dr) for doing the circle integration
- inout.plotfit: set to 1 to plot the resulting fittings, and to 0 otherwise.
- inout.crit_conc: critical threshold fluorescense to define a threshold-based characteristic size.


Data structure important fields

data.regions{index}
data.regions{index}.rs : radii for representing the intensity profile and so on. Defined in angular_integration2 function.
