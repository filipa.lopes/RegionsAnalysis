function plot_Int_vs_r(inout,data,regionnumber,data_set,namestr)

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;
rs=data.region{ii}.rs;

ymax=1.1*max(max(data_set(:,:)));
ymin=0;
xmin=0;xmax=max(rs)*1.1;

if isfield(inout,'gradient_marker_max')
    ymax=inout.gradient_marker_max;
end

%frac_angularregion=data.region{ii}.frac_angularregion;

%maximumradius=data.region{ii}.radius;
%radiusstep=data.region{ii}.radiusstep;
%origin=data.region{regionnumber}.origin;
%x0=origin(1);y0=origin(2);

dirout=strcat('region_',num2str(regionnumber));

font=18;
%name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.twochannels_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep));
%name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.selected_channel_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep),'.pdf');
name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_',data.selected_channel_string);

outfilename=fullfile(data.outdatapath,dirout,name);



radial_concentrations=zeros(length(rs),data.time_frames);
%

if data.microns_per_pixel==1
    xlab='r [A.U.]';
else
    xlab='r [\mum]';
end
ylab='Fluorescense [A.U.]';


ind=0; hwait=waitbar(0,'% of progress for movie generation in circular region'); % generates a waitbar
for k=1:data.time_frames
    waitbar(1.0*ind/(data.time_frames-1)); ind=ind+1; 
    h=figure();
    ys=data_set(:,k);
    plot(rs,ys,'o','linewidth',2) % plotting the intensity
    axis([xmin xmax ymin ymax])
    xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
    set(gca,'fontSize',font);
    save2pdf(outfilename,h);
end

close(hwait);




