function [data]=plot_all_regions_intensities(inout,data,list_of_regions);

font=18;
namestr='avInt';


strchannels={'signal','marker'};
time_points=zeros(data.time_frames);

% Timecourse analysis

if length(time_points)>1
    
%
if inout.plot_time_in_frames==1
    xlab='t [frames]';
    time_points=1:data.time_frames;
else
    xlab='t [hours]';
    time_points=data.time_points_in_hours;
end

for numchan=1:2
strchann=strchannels{numchan};
h=figure();
name=strcat(namestr,'_','allregions','_',strchann);

outfilename=fullfile(data.outdatapath,name);

for i=1:length(list_of_regions) 
regionnumber=list_of_regions(i);
ii=regionnumber;
ys=data.region{ii}.mean_intensity(:,numchan);
%plot(time_points,ys,'-o','linewidth',1,'Color','Blue') % plotting the intensity
%plot(time_points,ys,'-','linewidth',1,'Color','Blue') % plotting the intensity

smoothening_window_width=3;
%ys=smooth_median_timecourse_vect(ys,smoothening_window_width)
if isfield(inout,'smooth_timecourse')
    if inout.smooth_timecourse==1
        ys=smooth_mean_timecourse_vect(ys,smoothening_window_width);
    end
end

plot(time_points,ys,'-','linewidth',1) % plotting the intensity

hold on
end

%ylim([550 900])
xlabel(xlab,'fontsize',font); ylabel('Average Fluorescense [A.U.]','fontsize',font)
set(gca,'fontSize',font);
save2pdf(outfilename,h);

end


% Plotting ratio

strchann='ratio_signal_to_marker';
h=figure();
name=strcat(namestr,'_','allregions','_',strchann);

outfilename=fullfile(data.outdatapath,name);

for i=1:length(list_of_regions) 
regionnumber=list_of_regions(i);
ii=regionnumber;
ys=data.region{ii}.mean_intensity_ratio(:);
%plot(time_points,ys,'-o','linewidth',1,'Color','Blue') % plotting the intensity
%plot(time_points,ys,'-','linewidth',1,'Color','Blue') % plotting the intensity

smoothening_window_width=3;
%ys=smooth_median_timecourse_vect(ys,smoothening_window_width)
if isfield(inout,'smooth_timecourse')
    if inout.smooth_timecourse==1
        ys=smooth_mean_timecourse_vect(ys,smoothening_window_width);
    end
end

plot(time_points,ys,'-','linewidth',1) % plotting the intensity

hold on
end

%ylim([550 900])
xlabel(xlab,'fontsize',font); ylabel('Average Fluorescense [A.U.]','fontsize',font)
set(gca,'fontSize',font);
save2pdf(outfilename,h);

end


% Statis data analysis
if length(time_points)==1
    xlab='Average Hes5 Fluorescense [A.U.]';
    ylab='Average Atoh1 Fluorescense [A.U.]';

xs=[];ys=[];

for i=1:length(list_of_regions) 
    
regionnumber=list_of_regions(i);
ii=regionnumber;
xs=[xs data.region{ii}.mean_intensity(:,1)];
ys=[ys data.region{ii}.mean_intensity(:,2)];

end

h=figure();

%plot(xs,ys,'o') % plotting the intensity

scatter(xs,ys)
ylim([0 2400])
xlim([0 5000])

name=strcat(namestr,'_','allregions');
outfilename=fullfile(data.outdatapath,name);

xlabel(xlab,'fontsize',font); ylabel(ylab,'fontsize',font)
set(gca,'fontSize',font);
save2pdf(outfilename,h);

% histos
nbins=10;

hh=histogram(xs);
xmax=max(xs);
hh.BinWidth=xmax/nbins;
hh.Normalization='probability';

xlabel(xlab); ylabel('Probability');
h1 = gcf;
set(gca,'fontSize',inout.fontsize);

name=strcat(namestr,'_Hes5_','allregions');
outfilename=fullfile(data.outdatapath,name);
save2pdf(outfilename,h1);


hh=histogram(ys);
xmax=max(ys);

hh.BinWidth=xmax/nbins;
hh.Normalization='probability';


xlabel(ylab); ylabel('Probability');
h2 = gcf;
set(gca,'fontSize',inout.fontsize);

name=strcat(namestr,'_Atoh1_','allregions');
outfilename=fullfile(data.outdatapath,name);
save2pdf(outfilename,h2);

end  % end of static snap

end