function plot_tc_features(data,regionnumber)

%ii=find(data.regionlist==regionnumber);
ii=regionnumber;
dirout=strcat('region_',num2str(regionnumber));
maxs=data.region{ii}.measures.maxs;
maxs_prod=data.region{ii}.measures.maxs_prod;

distr=data.region{ii}.measures.distr;
t_points=data.time_points_in_hours;
t_points_prod=data.time_ponts_in_hours_for_production;

font=18;
y_points={maxs distr maxs_prod};
x_points={t_points t_points t_points_prod};

file_lab={'mx_tc' 'distr_tc' 'mx_prod_tc'};
ylabels={'Maximal response [A.U.]' 'Distribution index [A.U.]' 'Maximal response production  [A.U. h^{-1}]' };
for k=1:length(y_points)
    yl=ylabels{k};
    h=figure();
    plot(x_points{k},y_points{k},'-r','linewidth',2)
    xlabel('Time [h]','fontsize',font); ylabel(yl,'fontsize',font);
    set(gca,'fontSize',font);
    
    name=strcat(file_lab{k},'_region',num2str(regionnumber),'_',data.twochannels_string);
    fgname=fullfile(data.datapath,dirout,strcat(file_lab{k},'.jpeg'))
    saveas(h,fgname);
end

%lastfm=data.region{ii}.radial_smooth_timecourse(:,end);
%(sum((lastfm.^2)/2)/sum(lastfm))/max(lastfm)

