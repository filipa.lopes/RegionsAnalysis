function plot_avInt_in_time(inout,data,regionnumber,data_set,namestr)
ii=regionnumber;

%ymax=1.1*max(max(data_set(:,:)));
%ymin=0;
%xmin=0;xmax=max(rs)*1.1;

dirout=strcat('region_',num2str(regionnumber));

font=18;
%name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.twochannels_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep));
%name=strcat(namestr,'_vs_r_','region',num2str(regionnumber),'_fracangle',num2str(frac_angularregion),'_',data.selected_channel_string,'_rmax',num2str(maximumradius),'_dr_',num2str(radiusstep),'.pdf');
name=strcat(namestr,'_','region',num2str(regionnumber),'_',data.selected_channel_string);

outfilename=fullfile(data.outdatapath,dirout,name);



time_points=zeros(data.time_frames);

%
if inout.plot_time_in_frames==1
    xlab='t [frames]';
    time_points=1:data.time_frames;
else
    xlab='t [hours]';
    time_points=data.time_points_in_hours;
end

h=figure();
ys=data_set;

plot(time_points,ys,'-o','linewidth',2) % plotting the intensity
%plot(time_points,ys,'-','linewidth',2) % plotting the intensity

%axis([xmin xmax ymin ymax])
xlabel(xlab,'fontsize',font); ylabel('Average Fluorescense [A.U.]','fontsize',font)
set(gca,'fontSize',font);
save2pdf(outfilename,h);
%ylim([400,800])

end