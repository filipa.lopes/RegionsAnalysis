function [data]=plot_regions_mean_features(inout,data,list_of_regions);


font=18;
namestr='avInt';
name=strcat(namestr,'_','allregions','_',data.selected_channel_string);

outfilename=fullfile(data.outdatapath,name);



time_points=zeros(data.time_frames);

%
if inout.plot_time_in_frames==1
    xlab='t [frames]';
    time_points=1:data.time_frames;
else
    xlab='t [hours]';
    time_points=data.time_points_in_hours;
end




h=figure();

for i=1:length(list_of_regions) 
regionnumber=list_of_regions(i);
ii=regionnumber;
ys=data.region{ii}.mean_intensity;
%plot(time_points,ys,'-o','linewidth',1,'Color','Blue') % plotting the intensity
%plot(time_points,ys,'-','linewidth',1,'Color','Blue') % plotting the intensity

smoothening_window_width=3;
%ys=smooth_median_timecourse_vect(ys,smoothening_window_width)
if isfield(inout,'smooth_timecourse')
    if inout.smooth_timecourse==1
        ys=smooth_mean_timecourse_vect(ys,smoothening_window_width);
    end
end

plot(time_points,ys,'-','linewidth',1) % plotting the intensity

hold on
end

ys=data.allregions_average_intensity;

%plot(time_points,ys,'-','linewidth',2,'Color','Red') % plotting the intensity
%plot(time_points,ys,'-o','linewidth',2,'Color','Red') % plotting the intensity

%axis([xmin xmax ymin ymax])
%ylim([550 900])
xlabel(xlab,'fontsize',font); ylabel('Average Fluorescense [A.U.]','fontsize',font)
set(gca,'fontSize',font);
save2pdf(outfilename,h);



end